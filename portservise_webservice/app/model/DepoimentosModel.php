<?
require_once("DB.php");
class DepoimentosModel {
	
	public function __construct() {

    }

	public function listDepoimentos(){
		$sql = "select * from depoimentos";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function listDepoimentosById($request){
		$sql = "select * from depoimentos where id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function insertDepoimentos($request){
		$data = array();
		try{
			$sql = "insert into depoimentos (nota,descricao,classificacao,servico_id,usuario_id) 
			values (:nota,:descricao,:classificacao,:servico_id,:usuario_id);";
			$insert = DB::prepare($sql);
			$insert->bindParam(":nota",$request['nota']);
			$insert->bindParam(":descricao",$request['descricao']);
			$insert->bindParam(":classificacao",$request['classificacao']);
			$insert->bindParam(":servico_id",$request['servico_id']);
			$insert->bindParam(":usuario_id",$request['usuario_id']);
			$insert->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}
	
	
	public function updateDepoimentos($request){
		$data = array();
		try{
			$sql = "update depoimentos set nota = :nota,descricao = :descricao, classificacao = :classificacao ,
			servico_id = :servico_id, usuario_id = :usuario_id where id = :id;";
			$insert = DB::prepare($sql);
			$insert->bindParam(":nota",$request['nota']);
			$insert->bindParam(":descricao",$request['descricao']);
			$insert->bindParam(":classificacao",$request['classificacao']);
			$insert->bindParam(":servico_id",$request['servico_id']);
			$insert->bindParam(":usuario_id",$request['usuario_id']);
			$insert->bindParam(":id",$request['id']);
			$insert->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;		
	}
	
	public function delteDepoimentosById($request){
		$data = array();
		try{
			$sql = "delete from depoimentos where id = :id";
			$consulta = DB::prepare($sql);
			$consulta->bindParam(":id",$request['id']);
			$consulta->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;	
}