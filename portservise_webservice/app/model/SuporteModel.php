<?
require_once("DB.php");
class SuporteModel{

    public function __construct() {
    }

	public function listSuporte($request){
		$sql = "select * from suporte";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function insertSuporte($request){
		$data = array();
		try{
			$sql = "insert into suporte (assunto,descricao,id_usuario) 
			values (:assunto,:descricao,:id_usuario);";
			$insert = DB::prepare($sql);
			$insert->bindParam(":assunto",$request['assunto']);
			$insert->bindParam(":descricao",$request['descricao']);
			$insert->bindParam(":id_usuario",$request['id_usuario']);
			$insert->execute();
			$request["id"] = DB::lastInsertId();
			$data["success"] = true;
			$data["Suporte"] = $request;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
		//return $request;
	}
	
}