<?php include 'head.php'; ?>
<body>
    <?php include 'nav_top.php';    ?>
    <hr class="topbar"/>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <br />
                <br />
                <br />
                <br />
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-md-11 col-sm-12">
                                <div class="well">
                                    <div class="alert alert-danger" id="danger_login" style="display:none;">
                                        <strong>Alerta!</strong>
                                        <span id="span_alerta_login"></span>
                                    </div>
                                    
                                    <h2>Entrar</h2>
                                    <p>
                                        Se você tem uma conta conosco , por favor faça login.</p>
                                        <form role="form" id="form_login_usuario">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input id="login_email"  name="email" type="text" class="form-control " placeholder="Enter email">
                                            </div>
                                            <div class="form-group">
                                                <label>Senha</label>
                                                <input id="login_senha" name="senha" type="password" class="form-control"placeholder="Password">
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox">
                                                    Lembre de mim
                                                </label>
                                            </div><br />
                                            <input id="login_usuario" type="button" class="btn btn-primary" value="Entrar" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-12 col-md-11 pull-right">
                                    <div class="well">
                                        <h2>
                                            Registe-se</h2>
                                            <p>Ao criar uma conta conosco, você tera acesso a diversas oportunidades de mostrar os seu serviçõs</p><br />
                                            <a href="register.php" class="btn btn-primary">Criar conta</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <script>
            $( document ).ready(function() {
                $('#modalLoginMenu').html('');
                $('#modalLogin').html('');
            });
            </script>
            <?php include 'footer.php'; ?>
            <script src="js/usuario.js"></script>
        </body>
        <!-- Mirrored from templates.expresspixel.com/bootlistings/my_account.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 12:16:18 GMT -->
        </html>