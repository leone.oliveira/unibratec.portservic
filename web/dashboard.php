<?php 
include 'verificar.php';
include 'head.php'; ?>
<body>
    <?php include 'nav_top.php'; ?>
    <hr class="topbar"/>
    <div class="container">
        <div class="row">
            <?php include "menu-dashboard.php" ?>
            <div class="col-sm-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Painel de controle</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3 col-xs-6" style="border-right: 1px solid #DADADA; text-align: center;">
                                <h3 style="text-align: center;">Contratos</h3>
                                <p>8</p>
                            </div>
                            <div class="col-sm-3 col-xs-6" style="border-right: 1px solid #DADADA; text-align: center;">
                                <h3 style="text-align: center;">Anúncios</h3>
                                <p>5</p>
                            </div>
                            <div class="col-sm-3 col-xs-6" style="border-right: 1px solid #DADADA; text-align: center;">
                                <h3 style="text-align: center;">Anúncios vistos no mês</h3>
                                <p>86</p>
                            </div>
                            <div class="col-sm-3 col-xs-6" style="text-align: center;">
                                <h3 style="text-align: center;">
                                    visualizações dos anúncios</h3>
                                    <p>257</p>
                                </div>
                            </div>
                            <br />
                            <br /><br />
                            <div class="row">
                                <div class="col-sm-4">
                                    <h3>Crie um novo anúncio</h3>
                                    <p>Clique no botão abaixo para começar a criar um novo anúncio. 
                                        Você pode enviar até 20 imagens por serviço e se destacar como profissinal.</p>
                                        <a href="account_ad_create.html" class="btn btn-default">Criar novo anúncio</a>
                                        <br />
                                        <br />
                                    </div>
                                    <div class="col-sm-4">
                                        <h3>Promover os seus anúncios</h3>
                                        <p>Se o seu anúncio caiu para a 3ª ou 4ª página, é um bom momento para obter um plano de volta
                                           para a 1ª página.</p>
                                           <a href="plano.php" class="btn btn-default" style="margin-top: 18px;">Escolher um plano</a>
                                           <br />
                                           <br />
                                       </div>
                                       <div class="col-sm-4">
                                        <h3>Solicitar feedback</h3>
                                        <p>Solicitando feedback em seus contratos faz de você um profissional mais elevado.</p>
                                        <a href="#" class="btn btn-default" style="margin-top: 36px;">Verificar contratos</a>
                                        <br />
                                        <br />
                                    </div>
                                </div>
                                <br />
                                <br />
                                <br />
                                <br />

                                <div class="row">
                                    <div class="col-sm-12" >
                                        <h3 style="text-align: center;">Anúncios visitados nas ultimas 24 horas</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="visualization"></div>
                                    </div>
                                </div>
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- Modal -->
            
            
            <?php include 'footer.php'; ?>
        </body>
        <!-- Mirrored from templates.expresspixel.com/bootlistings/account_dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 10:59:58 GMT -->
        </html>