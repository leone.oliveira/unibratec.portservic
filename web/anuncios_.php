<?php include 'head.php'; ?>
<body>
    <?php include 'nav_top.php'; 	?>

    <hr class="topbar"/>
    <div class="container">
        <div class="row">
            <?php include "menu-dashboard.php" ?>
            <div class="col-sm-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Meus Anúncios</div>
                    <div class="panel-body">
                        <form class="form-vertical">
                            <fieldset>

                                <div class="row">  
                                    <div class="col-sm-12" >

                                        <table class="table edit-listings">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th>Data&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_listAll">
											
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>

                        </fieldset>
                    </form>


                </div>
            </div>
        </div>
        <br />

    </div>
</div><!-- Modal -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="modalLogin" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Sign in to your account</h4>
            </div>
            <div class="modal-body">
                <p>If you have an account with us, please enter your details below.</p>

                <form method="POST" action="http://templates.expresspixel.com/bootlistings/account_dashboard.html" accept-charset="UTF-8" id="user-login-form" class="form ajax" data-replace=".error-message p">

                    <div class="form-group">
                        <input placeholder="Your username/email" class="form-control" name="email" type="text">                </div>

                        <div class="form-group">
                            <input placeholder="Your password" class="form-control" name="password" type="password" value="">                </div>

                            <div class="row">
                                <div class="col-md-6">

                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary pull-right">Login</button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <a data-toggle="modal" href="#modalForgot">Forgot your password?</a>
                                </div>
                            </div>

                        </form>
                    </div>

                    <div class="modal-footer" style="text-align: center">
                        <div class="error-message"><p style="color: #000; font-weight: normal;">Don't have an account? <a class="link-info" href="register.html">Register now</a></p></div>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <!-- Modal -->
        <div class="modal fade" id="modalForgot" tabindex="-1" role="dialog" aria-labelledby="modalForgot" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Forgot your password?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Enter your email to continue</p>

                        <form role="form">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Your email address">
                            </div>

                            <div class="row">
                                <div class="col-md-6">

                                </div><div class="col-md-6">
                                <a href="my_account.html" class="btn btn-primary pull-right">Continue</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	<script src="js/servico.js"></script>
    <?php include 'footer.php'; ?>

</body>

<!-- Mirrored from templates.expresspixel.com/bootlistings/account_ads.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 12:17:51 GMT -->
</html>