<?php if($qtd == 0){ ?>
<div class="col-md-4 col-sm-4 col-xs-12">
    <label class="lbl_registro">
        Nenhum registro recuperado
    </label>
</div>
<?php } else { ?>
<div class="col-md-4 col-sm-4 col-xs-12">
    <label class="lbl_registro">
        <?php print($qtd) ?> registro(s) | Mostrando de <?php print($de) ?> a <?php print($ate) ?>
    </label>
</div>
<div class="col-md-4 col-sm-4 col-xs-12 pull-right">
    <ul class="pagination pull-right">
        <li><a href="#" onClick="Pesquisar('<?php print($ordem) ?>', '1','N');">«</a></li>
        <?php $totalPaginas = (int) ($qtd / $_SESSION['qtdList']);
            if($totalPaginas == 0){
                $totalPaginas = 1;
            } 
            for ($i=1; $i <= $totalPaginas; $i++) { 
        ?>
            <!--<li><a class="active" href="#">1</a></li> -->
            <li <?php if($pag == $i){ ?> class="active" <?php } ?>><a href="#" onClick="Pesquisar('<?php print($ordem) ?>', '<?php print($i) ?>','N');" ><?php echo $i; ?></a></li>
        <?php } ?>
        <li><a href="#" onClick="Pesquisar('<?php print($ordem) ?>', '<?php print($totalPaginas) ?>','N');">»</a></li>
    </ul>
</div>
<?php } ?>