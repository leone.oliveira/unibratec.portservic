var plano = {
	construct:$(function(){
		plano.listPlanos();
	}),
	listPlanos:function(){
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/PlanoModel/listPlanos",
			type:"POST",
			dataType:"JSON",
			success:function(data){
				console.log(data);
				for(var i = 0; i < data.length; i++){
					$("#codplano").append("<option value="+data[i].plano_id+">"+data[i].nome+"</option>");	
				}
			},
			error:function(data){
				
			}
		});
	},
	usuarioListPlanosById:function(codplano){
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/PlanoModel/listPlanosById",
			type:"POST",
			dataType:"JSON",
			data:{codplano:codplano},
			success:function(data){
				console.log(data);
				for(var i = 0; i < data.length; i++){
					$("#nm_plano_usuario").append(data[i].nome);
					$("#ds_plano_usuario").append(data[i].descricao);
				}
			},
			error:function(data){
				
			}
		});		
	},
	aderirPlano:function(){
		var serialize = $("#form_plano").serialize();
		var enviarEmail = {};
		var codformapagamento = $('input[name="codformapagamento"]:checked').val();
		enviarEmail.codusuario = $("#codusuario").val();
		enviarEmail.codusuario = $("#codusuario").val();
		enviarEmail.msgEmail = "aderirPlano";
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/PlanoModel/aderirPlano",
			type:"POST",
			dataType:"JSON",
			data:serialize,
			success:function(data){
				console.log(data);
				$("#msgok").html("Agora é so esperar o pagamento ser aprovado e desfrutar!"); 
				$('#alerta-plano-sucesso').css({"display":"block"});
				$.ajax({
					url: "../web/util/enviar_emails.php",
					type: "GET",
					//dataType:"JSON",
					data:enviarEmail,
					success:function(result){
						//console.log(result);
					},
					error:function(result){
						console.log(result);
					}
				});
				if(codformapagamento == 3){
					//var newWindow = window.open("","_blank");
					$.ajax({
						url: "../web/util/boleto_santander_banespa.php",
						type: "GET",
					//	dataType:"JSON",
						data:serialize,
						success:function(result){
							var newWindow = window.open("","_blank");
							//$(newWindow.document.body).html(result);
							newWindow.document.write(result);
						},
						error:function(result){
							console.log(result);
						}
					});
				}
			},
			error:function(data){
				console.log(data);
				$("#alerta-plano-erro").css({"display":"block"});
				$("#msgAlerta").html("Houve algum problema no pagamento favor entrar em contato com o suporte."); 
			}
		});		
	},
	detalhePlanos:function(codplano){
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/PlanoModel/listPlanosById",
			type:"POST",
			dataType:"JSON",
			data:{codplano:codplano},
			success:function(data){
				console.log(data);
				for(var i = 0; i < data.length; i++){
					$("#nm_plano").html(" R$ "+data[i].preco);
					$("#valor").val(data[i].preco);
					$("#valorbase").val(data[i].preco);
					$("#ds_plano").html(data[i].descricao);
					$("#dias").val(30);
					$("#qtdanuncio").val(data[i].qtdanuncio);
				}
			}
		});
	}
}

plano.construct;