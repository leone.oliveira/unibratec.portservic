<?php
    include 'verificar.php'; 
    include 'head.php'; ?>
<body>
    <?php 
	include 'nav_top.php'; 
	?>
	
	
    <hr style="border: 3px solid #D2160A"/>
    <div class="container">
        <div class="row">
            <?php include "menu-dashboard.php" ?>
            <div class="col-sm-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Perfil</div>
                    <div class="panel-body">
                        <form class="form-horizontal" id="prefil_usuario">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="foto"  class="col-sm-12 control-label">Sua foto</label>
											<div id="infosuccess" style="display:none" class="alert alert-info">
												<strong>Aviso!</strong>
												<span id="infoSpansuccess"></span>
											</div>
											<div id="infoerror" style="display:none" class="alert alert-danger">
												<strong>Aviso!</strong> 
												<span id="infoSpanerror"></span>
											</div>
                                            <div class="col-sm-11">
												<form enctype="multipart/form-data">
                                                <img id="file-select" id="foto" name="foto" src="<?= empty($_SESSION['usuario']['foto']) ? 'css/images/180x100.png': 'uploadFile/'.$_SESSION['usuario']['foto'];?>" width="100%" /><br />
                                                <span class="btn btn-default btn-file">
                                                    Clique para fazer upload <input id="js-upload-files" type="file">
                                                </span>
												</form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <br />
                                        <div class="form-group">
                                            <label for="nome" class="col-sm-4 control-label">Nome</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="nome" name="nome" value="<?= $_SESSION['usuario']['nome']?>" placeholder="Primeiro nome">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nick" class="col-sm-4 control-label">Nick</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="nick" name="nick" value="<?= $_SESSION['usuario']['nick']?>" placeholder="Como deseja ser chamado">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="sobre" style="width: 100%" class="control-label">Sobre você</label><br /><br />
                                        <textarea id="sobre" name="sobre" class="form-control col-sm-8" rows="6" style="width: 99%"><?= $_SESSION['usuario']['sobre'];?></textarea>
                                    </div>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <label for="cpf_cnpj" class="col-sm-2 control-label">CPF / CNPJ</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="cpf_cnpj" id="cpf_cnpj" value="<?= $_SESSION['usuario']['cpf_cnpj']?>" placeholder="Informe seu CPF ou CNPJ">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail1" class="col-sm-2 control-label">Sexo</label>
                                    <div class="col-sm-10">
                                    <label class="radio-inline" for="radios-1">
                                        <input type="radio" name="sexo" id="radios-1" value="M" <? if($_SESSION['usuario']['sexo'] == 'M'){ echo 'checked';};?>>
                                        Masculino
                                    </label>
                                    <label class="radio-inline" for="radios-2">
                                        <input type="radio" name="sexo" id="radios-2" value="F" <? if($_SESSION['usuario']['sexo'] == 'F'){ echo 'checked';};?>>
                                        Feminino
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="cep" class="col-sm-2 control-label">CEP</label>
                                    <div class="col-sm-10">
                                        <input id="cep" name="cep" type="text" class="form-control " value="<?= $_SESSION['endereco']['cep'];?>" />
                                    </div>
                                </div>
								<div class="form-group">
									<label for="cidade" class="col-sm-2 control-label">Cidade</label>
                                    <div class="col-sm-10">
                                        <input id="cidade" name="cidade" type="text" class="form-control " value="<?= $_SESSION['endereco']['cidade'];?>" />
                                    </div>
                                </div>
								<div class="form-group">
									<label for="bairro" class="col-sm-2 control-label">Bairro</label>
                                    <div class="col-sm-10">
                                        <input id="bairro" name="bairro" type="text" class="form-control " value="<?= $_SESSION['endereco']['bairro'];?>" />
                                    </div>
                                </div>								
								<div class="form-group">
									<label for="uf" class="col-sm-2 control-label">Estado</label>
                                    <div class="col-sm-10">
										<select name="uf" id="uf" class="form-control ">
											<option value="">Selecione</option>
											<option value="AC">Acre</option>
											<option value="AL">Alagoas</option>
											<option value="AP">Amapá</option>
											<option value="AM">Amazonas</option>
											<option value="BA">Bahia</option>
											<option value="CE">Ceará</option>
											<option value="DF">Distrito Federal</option>
											<option value="ES">Espirito Santo</option>
											<option value="GO">Goiás</option>
											<option value="MA">Maranhão</option>
											<option value="MT">Mato Grosso</option>
											<option value="MS">Mato Grosso do Sul</option>
											<option value="MG">Minas Gerais</option>
											<option value="PA">Pará</option>
											<option value="PB">Paraiba</option>
											<option value="PR">Paraná</option>
											<option value="PE">Pernambuco</option>
											<option value="PI">Piauí</option>
											<option value="RJ">Rio de Janeiro</option>
											<option value="RN">Rio Grande do Norte</option>
											<option value="RS">Rio Grande do Sul</option>
											<option value="RO">Rondônia</option>
											<option value="RR">Roraima</option>
											<option value="SC">Santa Catarina</option>
											<option value="SP">São Paulo</option>
											<option value="SE">Sergipe</option>
											<option value="TO">Tocantis</option>
										 </select>
                                    </div>
                                </div>
								<div class="form-group">
									<label for="endereco" class="col-sm-2 control-label">Rua</label>
                                    <div class="col-sm-10">
										<input id="endereco" name="endereco" type="text" class="form-control " value="<?= $_SESSION['endereco']['endereco'];?>" />
									</div>
                                </div>	
								<div class="form-group">
									<label for="numero" class="col-sm-2 control-label">Numero</label>
                                    <div class="col-sm-10">
										<input id="numero" name="numero" type="text" class="form-control " value="<?= $_SESSION['endereco']['numero'];?>" />
									</div>
                                </div>								
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="email" name="email" value="<?= $_SESSION['usuario']['email']?>"  placeholder="Deve ser válido, vamos enviar-lhe uma mensagem de validação">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="celular" class="col-sm-2 control-label">Celular</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" maxlength="16" id="celular" name="celular" value="<?= $_SESSION['usuario']['celular']?>" placeholder="O teu número de telemóvel">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="telefone" class="col-sm-2 control-label">Telefone</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" maxlength="16" id="telefone" name="telefone" value="<?= $_SESSION['usuario']['telefone']?>" placeholder="Não se esqueça de incluir o código de área">
                                    </div>
                                </div>
                                <br />
								<input type="hidden" name="id" id="id" value="<?= $_SESSION['usuario']['id']?>"/>
								<input type="hidden" id="id_endereco" name="id_endereco"  value="<?= $_SESSION['endereco']['id'];?>" />
								<input type="hidden" name="senha" id="senha" value="<?= $_SESSION['usuario']['senha']?>"/>
								<input type="hidden" name="foto" id="foto" value="<?= $_SESSION['usuario']['foto']?>"/>
                                <input type="button" id="btPerfil" class="btn btn-primary" value="Salvar"/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
</fieldset>
</form>
</div>
</div>
</div>
<br />
</div>
</div>
<script src="js/usuario.js"></script>
<script src="js/mascara.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#uf").val('<?= $_SESSION['endereco']['uf'];?>');
	$("#cep").mask("99999-999");
});
</script>
<script>
	usuario.listEndereco(<?= $_SESSION['usuario']['id'];?>);
	usuario.uploadFotoperfil(<?= $_SESSION['usuario']['id'];?>);
</script>
<?php include 'footer.php'; ?>
</body>
<!-- Mirrored from templates.expresspixel.com/bootlistings/account_profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 12:17:51 GMT -->
</html>